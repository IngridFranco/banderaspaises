PROYECTO BANDERAS DE PAISES
==============================

Descripcion del proyecto
---------------------------

Este proyecto se trata de listar los paises con sus respectivas banderas, sus nombres y capital.
Tambien muestra otra actividad en la cual se visualiza el video del himno de cada uno de los paises.
Dichas banderas eh himnos estan cargadas mediante una url:

ListDatos.add(new pais("El Salvador","San Salvador","<iframe width=\"400\" height=\"300\" src=\"https://www.youtube.com/embed/UnWzKEO6WnM\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>","https://www.youtube.com/embed/UnWzKEO6WnM","https://www.ipandetec.org/wp-content/uploads/2018/01/El-Salvador.png"));

Esta url permite cargar de manera instantanea las imagenes de los distintos paises
Tambien permite cargar los videos embed a través de otra actividad.
