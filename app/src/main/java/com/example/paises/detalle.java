package com.example.paises;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.example.paises.enetity.pais;
import com.squareup.picasso.Picasso;

public class detalle extends AppCompatActivity  {

    String direccion;
    TextView nombre;
    WebView webView;
    ImageView img;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalle);


        //Codigo para hacer pantalla emergente
        DisplayMetrics medidaVentana = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(medidaVentana);

        int ancho = medidaVentana.widthPixels;
        int alto = medidaVentana.heightPixels;

        getWindow().setLayout((int)(ancho * 0.85), (int)(alto * 0.7));
        // cierra codigo para pantalla emergente

        nombre=(TextView) findViewById(R.id.nombrePais);
        img=(ImageView) findViewById(R.id.pais);
        webView=(WebView) findViewById(R.id.video);

        direccion = "";


        Bundle recibe=getIntent().getExtras();
        pais p = (pais) recibe.getSerializable("Pais");
        nombre.setText(p.getNombre().toString());
        webView.loadData(p.getVideoUrl(),"text/html","utf-8");
        Picasso.with(img.getContext()).load(p.getUrl()).into(img);

        webView.getSettings().setJavaScriptEnabled(true);
        webView.setWebChromeClient(new WebChromeClient() {
        });
        webView.loadData(p.getVideoUrl(), "text/html" , "utf-8");
      //  tx.setText(direccion);
        //tx.setOnClickListener(this);

    }


    public void irWeb(String d){
        Uri uri = Uri.parse(d);
        Intent intenNav =   new Intent(Intent.ACTION_VIEW,uri);
        startActivity(intenNav);

    }
}
